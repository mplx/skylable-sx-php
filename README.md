# Skylable SX PHP Client

A standalone PHP library for interacting with Skylable SX (a distributed object-storage software for data clusters by http://www.skylable.com).

[![Build Status](https://travis-ci.org/mplx/skylable-sx-php.svg?branch=master)](https://travis-ci.org/mplx/skylable-sx-php)

## Installation

The library is listed at [packagist](packagist.org/packages/mplx/skylablesx) and should be installed with composer.

```json
{
    "require": {
        "mplx/skylablesx": "dev-master"
    }
}
```

Source code is available at [github](github.com/mplx/skylable-sx-php) and [bitbucket](bitbucket.org/mplx/skylable-sx-php).

## Usage

```php
$sx = new Sx('__YOUR_KEY__', '__YOUR_SX_CLUSTER__');
$sx = new Sx('__YOUR_KEY__', '__YOUR_SX_CLUSTER__', __YOUR_TCP__PORT__);
```

```php
$sx = new Sx();
$sx->setAuth('__YOUR_KEY__');
$sx->setEndpoint('__YOUR_SX_CLUSTER__');
$sx->setPort(__YOUR_TCP__PORT__);
```

```php
$sx->setSSL(false);         // do not use SSL
$sx->setSSL(true);          // SSL, verify certificate
$sx->setSSL(true, false);   // SSL, do not verify certificate
```

## Operations

### getNodeList

```php
$nodes = $sx->getNodeList();
```

```php
// List all nodes
echo PHP_EOL . 'Available nodes:' . PHP_EOL;
$nodes = $sx->getNodeList();
foreach ($nodes->nodeList as $node => $ip) {
    echo 'Node #' . $node . ': IP ' . $ip . PHP_EOL;
}
```

### getVolumeList

```php
$volumes = $sx->getVolumeList();
```

```php
// List all volumes
$samplevol = null;
$volumes = $sx->getVolumeList();
echo PHP_EOL . 'Available volumes:' . PHP_EOL;
foreach ($volumes->volumeList as $vol => $meta) {
    $samplevol = $vol;
    echo $vol . ' (replicas=' . $meta->replicaCount . ', maxRev=' . $meta->maxRevisions . ')' . PHP_EOL;
}
```

### locateVolume

```php
$locate = $sx->locateVolume($volume);
```

```php
// Locate volume
$samplevol = 'playground';
echo PHP_EOL . 'Locate volume: ' . $samplevol . PHP_EOL;
$locate = $sx->locateVolume($samplevol);
foreach ($locate->nodeList as $node => $ip) {
    echo 'Node #' . $node . ': IP ' . $ip . PHP_EOL;
}
```

### getUserList

```php
$users = $sx->getUserList();
```

```php
// List all users
$users = $sx->getUserList();
echo PHP_EOL . 'List all users:' . PHP_EOL;
foreach ($users as $user => $properties) {
    echo $user . PHP_EOL;
}
```

### getFileList

```php
$files = $sx->getFileList($volume);
$files = $sx->getFileList($volume, $filter);
```

```php
// List all files in volume $samplevol
$samplevol = 'playground';
$files = $sx->getFileList($samplevol);
echo PHP_EOL . 'List files on volume ' . $samplevol .':' . PHP_EOL;
foreach ($files->fileList as $file => $meta) {
    echo $file . PHP_EOL;
}
```

```php
// List all *.deb files in volume $samplevol
$samplevol = 'playground';
$samplefilter = '*.deb';
$files = $sx->getFilesList($samplevol, $samplefilter);
echo PHP_EOL . 'List ' . $samplefilter . ' files on volume ' . $samplevol .':' . PHP_EOL;
foreach ($files->fileList as $file => $meta) {
    echo $file . PHP_EOL;
}
```

### getFileMetadata

```php
$meta = $sx->getFileMetadata($volume, $file)
```

### getJobStatus

```php
$status = $sx->getJobStatus($requestId, $minPollInterval, $maxPollInterval, $maxRetries);
```

### getFile

```php
$file = $sx->fetchFile($volume, $file);
```

```php
// Fetch file to PHP var
$samplevol = 'playground';
$samplefile = 'sx_0.3-1~wheezy_amd64.deb';
echo PHP_EOL . 'Fetching file ' . $samplevol . $samplefile . PHP_EOL;
$file = $sx->fetchFile($samplevol, $samplefile);
```

### downloadFile

```php
$result = $sx->downloadFile($volume, $file, $target);
```

```php
// Download file to local filesystem
$samplevol = 'playground';
$samplefile = '/sx_0.3-1~wheezy_amd64.deb';
$sampletarget = './sx_0.3-1~wheezy_amd64.deb';
$result = $sx->downloadFile($samplevol, $samplefile, $sampletarget);
```

### deleteFile

```php
$result = $sx->deleteFile($volume, $file);
```

```php
// Delete file
$samplevol = 'playground';
$samplefile = '/sx_0.4-1~wheezy_amd64.deb';
echo PHP_EOL . 'Delete file ' . $samplevol . $samplefile . PHP_EOL;
$result = $sx->deleteFile($samplevol, $samplefile);
while ($result->requestStatus == 'PENDING') {
    echo "Delete pending... " . PHP_EOL;
    sleep(1);
    $result = $sx->getJobStatus($result->requestId, 1, 2, 1);
}
if ($result->requestStatus == 'OK') {
    echo "Sucessfully deleted file" . PHP_EOL;
} elseif ($result->requestStatus == 'ERROR') {
    echo "Error deleting file: " . $result->requestMessage . PHP_EOL;
}
```

### putFile

```php
$result = putFile($volume, $file, $body);
```

```php
// Upload ("put") file (file content from PHP var)
$samplevol = 'playground';
$samplefile = '/upload.txt';
$samplecontent = 'dynamic text: ' . microtime();
echo PHP_EOL . 'Putting file ' . $samplevol . $samplefile . PHP_EOL;
$result = $sx->putFile($samplevol, $samplefile, $samplecontent);
while ($result->requestStatus == 'PENDING') {
    echo "Upload pending... " . PHP_EOL;
    sleep(1);
    $result = $sx->getJobStatus($result->requestId, 1, 2, 1);
}
if ($result->requestStatus == 'OK') {
    echo "Upload sucessfull" . PHP_EOL;
} elseif ($result->requestStatus == 'ERROR') {
    echo "Upload failed: " . $result->requestMessage . PHP_EOL;
}
```

### uploadFile

```php
$result = uploadFile($volume, $file, $source);
```

```php
// Upload file (file from filesystem)
$source = './sx_0.9-1-1~wheezy_amd64.deb';
$samplevol = 'playground';
$target = '/sx_0.9-1-1~wheezy_amd64.deb';
echo PHP_EOL . 'Uploading file ' . $source . ' to volume ' . $samplevol . $target . PHP_EOL;
$result = $sx->uploadFile($samplevol, $target, $source);
while ($result->requestStatus == 'PENDING') {
    echo "Upload pending... " . PHP_EOL;
    sleep(1);
    $result = $sx->getJobStatus($result->requestId, 1, 2, 1);
}
if ($result->requestStatus == 'OK') {
    echo "Upload sucessfull" . PHP_EOL;
} elseif ($result->requestStatus == 'ERROR') {
    echo "Error uploading file: " . $result->requestMessage . PHP_EOL;
}
```

## License

Licensed under the [BSD 3-Clause License](LICENSE.txt).
