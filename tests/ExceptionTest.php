<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx\Tests;

use mplx\skylablesx\SxException;

class ExceptionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException        \mplx\skylablesx\SxException
     * @expectedExceptionMessage mplx
     */
    public function testException()
    {
        throw new SxException('mplx', __FILE__, __LINE__, 1);
    }
}
