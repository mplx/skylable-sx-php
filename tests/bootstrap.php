<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

// report all errors
error_reporting(E_ALL);

// autoloader
$loader = require __DIR__ . '/../vendor/autoload.php';

// timezone
date_default_timezone_set('UTC');
