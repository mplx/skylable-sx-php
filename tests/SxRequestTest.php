<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx\Tests;

use mplx\skylablesx\Sx;
use mplx\skylablesx\SxRequest;
use mplx\skylablesx\SxException;

class SxRequestTest extends \PHPUnit_Framework_TestCase
{
    // sx object
    private $sx = null;
    private $rest = null;

    // setup sx
    public function setUp()
    {
        $this->sx = new Sx();
        $this->rest = $this->sx->newRequest();
    }

    // blockPadding()
    public function testBlockPadding()
    {
        $this->assertEquals(
            '123',
            $this->rest->blockPadding('123', 3)
        );
        $this->assertEquals(
            '123' . chr(0) . chr(0),
            $this->rest->blockPadding('123', 5)
        );
        $this->assertEquals(
            '123AAAAAAA',
            $this->rest->blockPadding('123', 10, 'A')
        );
    }

    // blockPadding Exception
    public function testBlockPaddingException()
    {
        $this->setExpectedException('mplx\skylablesx\SxException');
        $this->rest->blockPadding('1234567890', 5);
    }

    // strPrefix()
    public function testStrPrefix()
    {
        $this->assertEquals('/test', $this->rest->strPrefix('test'));
        $this->assertEquals('-test', $this->rest->strPrefix('test', '-'));
        $this->assertEquals('/test', $this->rest->strPrefix('/test'));
        $this->assertEquals('?test', $this->rest->strPrefix('?test'));
    }

    // setEndpoint
    public function testSetEndpoint()
    {
        $this->assertTrue($this->rest->setEndpoint('dummy.tld'));
    }
}
