<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx\Tests;

use mplx\skylablesx\Sx;
use mplx\skylablesx\SxRequest;
use mplx\skylablesx\SxException;
use mplx\skylablesx\SxCurl;

class SxCurlTest extends \PHPUnit_Framework_TestCase
{
    // sx object
    private $curl = null;
    private $rest = null;

    // setup sx
    public function setUp()
    {
        $this->curl = new SxCurl();
    }

    public function testCreateClose()
    {
        $curl = new SxCurl('dummy.tld', true);
        $curl->close();
    }

    public function testSetSSLverify()
    {
        $this->assertTrue($this->curl->setSSLverify(true));
        $this->assertTrue($this->curl->setSSLverify(false));
    }

    public function testSetRequestMethod()
    {
        $this->assertTrue($this->curl->setRequestMethod('GET'));
        $this->assertTrue($this->curl->setRequestMethod('PUT'));
        $this->assertTrue($this->curl->setRequestMethod('POST'));
        $this->assertTrue($this->curl->setRequestMethod('DELETE'));
    }

    public function testSetRequestMethodException()
    {
        $this->setExpectedException('mplx\skylablesx\SxException');
        $this->curl->setRequestMethod('invalid');
    }

    public function testSetHeader()
    {
        $this->curl->setHeader(array('User-Agent: mplx/unittest'));
    }

    public function testSetBody()
    {
        $this->curl->SetBody(json_encode(array('test')));
    }
}
