<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx\Tests;

use mplx\skylablesx\Sx;

/**
* Test SX authorization
*
* @link https://wiki.skylable.com/wiki/REST_API_Queries
*/
class AuthorizationTest extends \PHPUnit_Framework_TestCase
{
    // content for PUT test
    private $content = null;

    // sx object
    private $sx = null;

    // rest request object
    private $rest = null;

    // setup up
    public function setUp()
    {
        // sx + request
        $this->sx = new Sx('RcVxoVbdzvQTUacTvN3uW6fpVGBgIINMU1Zwqx2ob3aXJBy0Orb7kAAA', 'dummy.tld');
        $this->rest = $this->sx->newRequest();
    }

    // sx user key
    public function testUserKey()
    {
        $this->assertEquals(bin2hex($this->sx->getKey()), '6020834c535670ab1da86f7697241cb43ab6fb90');
    }

    // setAuth: sx user key
    public function testSetAuthGetKey()
    {
        $this->sx->setAuth('RcVxoVbdzvQTUacTvN3uW6fpVGBgIINMU1Zwqx2ob3aXJBy0Orb7kAAA');
        $this->assertEquals(bin2hex($this->sx->getKey()), '6020834c535670ab1da86f7697241cb43ab6fb90');
    }

    // hmac sha1
    public function testHmacSha1()
    {
        $hmacsha1 = $this->rest->hmacSha1(
            hex2bin('6020834c535670ab1da86f7697241cb43ab6fb90'),
            "GET\n?nodeList\nThu, 10 Jul 2014 13:35:32 GMT\nda39a3ee5e6b4b0d3255bfef95601890afd80709\n"
        );
        $this->assertEquals($hmacsha1, '96f852a08d3ee255b77364a6bbd106be0dc0e44e');
    }

    // sx signing key without empty body
    public function testSignKey()
    {
        $signkey = $this->rest->signRequest('GET', '?nodeList', 'Thu, 10 Jul 2014 13:35:32 GMT', sha1(''));
        $this->assertEquals(
            $signkey,
            '45c571a156ddcef41351a713bcddee5ba7e9546096f852a08d3ee255b77364a6bbd106be0dc0e44e0000'
        );
    }

    // sx signing key with body
    // requires different sx auth than other samples
    public function testSignKeyBody()
    {
        $sx = new Sx('4Ac1+cEYNjMotLejE1X7p5v4yTWuUOu9apPie0AlvT1mkgzJtjFHzwAA', 'cluster.tld', 80);
        $sx->setSSL(false);
        $rest = $sx->newRequest();

        $payload = '{"fileSize":15,"fileData":["0809ca51c30abd6da28993a7f690d78525c36ead"]}';
        $signkey = $rest->signRequest(
            'PUT',
            'test/mplx/sx-php-test.txt',
            'Tue, 24 Mar 2015 15:34:02 GMT',
            sha1($payload)
        );

        $this->assertEquals(
            $signkey,
            bin2hex(base64_decode('4Ac1+cEYNjMotLejE1X7p5v4yTVMj/xt5CKN2fTWkFr/esWe9Db0CQAA'))
        );
    }

    // http authorization header
    public function testAuthHeader()
    {
        $header = $this->rest->createAuthHeader(
            '45c571a156ddcef41351a713bcddee5ba7e9546096f852a08d3ee255b77364a6bbd106be0dc0e44e0000'
        );
        $this->assertEquals(
            $header,
            'Authorization: SKY RcVxoVbdzvQTUacTvN3uW6fpVGCW+FKgjT7iVbdzZKa70Qa+DcDkTgAA'
        );
    }

    // http date header
    public function testDateheader()
    {
        $header = $this->rest->createDateHeader('Thu, 10 Jul 2014 13:35:32 GMT');
        $this->assertEquals($header, 'Date: Thu, 10 Jul 2014 13:35:32 GMT');
    }
}
