<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx\Tests;

use mplx\skylablesx\Sx;

class SxTest extends \PHPUnit_Framework_TestCase
{
    // sx object
    private $sx = null;

    // setup sx
    public function setUp()
    {
        $this->sx = new Sx();
    }
    // SetSSL(): empty/invalid data
    public function testSetSSL()
    {
        $this->assertTrue($this->sx->setSSL(true, false));
        $this->assertTrue($this->sx->setSSL(false, true));
        $this->assertFalse($this->sx->setSSL('invalid', false));
    }

    // setAuth: empty/invalid data
    public function testSetAuthEmpty()
    {
        $this->assertFalse($this->sx->setAuth(''));
        $this->assertFalse($this->sx->setAuth(null));
        $this->assertFalse($this->sx->setAuth('invalid!'));
    }

    // setEndpoint
    public function testSetEndpoint()
    {
        $this->assertEquals(
            'cluster.dummy.tld:80',
            $this->sx->setEndpoint('cluster.dummy.tld', null)
        );
        $this->assertEquals(
            'cluster.dummy.tld:8443',
            $this->sx->setEndpoint('cluster.dummy.tld', 8443)
        );
    }

    // setPort
    public function testSetPort()
    {
        $this->assertFalse($this->sx->setPort('invalid!'));
        $this->assertEquals(8443, $this->sx->setPort(8443));
    }
}
