<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx;

/**
* Sx connection class
*/
class Sx
{
    /**
    * SX user id (binary presentation)
    *
    * @var string
    */
    private static $binUserId;

    /**
    * SX user key (binary presentation)
    *
    * @var string
    */
    private static $binUserKey;

    /**
    * SX connection endpoint
    *
    * @var string
    */
    private static $endpoint;

    /**
    * SX tcp port
    *
    * @var integer
    */
    private static $port;

    /**
    * use SSL for SX cluster connection
    *
    * @var boolean
    */
    private static $ssl = true;

    /**
    * verify certificates in SSL connections
    *
    * @var boolean
    */
    private static $sslverify = true;

    /**
    * Constructor
    *
    * @param string $token Access token
    * @param string $endpoint SX connection endpoint
    * @param integer $port TCP Port
    */
    public function __construct($token = null, $cluster = null, $port = null)
    {
        if ($token !== null) {
            self::setAuth($token);
        }

        if ($cluster !== null) {
            self::setEndpoint($cluster);
        }

        if ($cluster !== null) {
            self::setPort($port);
        } else {
            self::setPort(80);
        }
    }

    /**
    * New SX request
    *
    * @param string $node SX node
    * @return mixed
    */
    public function newRequest($node = false)
    {
        $endpoint = ($node) ? $node : self::$endpoint;
        $endpoint .= ':' . self::$port;
        return new SxRequest(self::$binUserId, self::$binUserKey, $endpoint, self::$ssl, self::$sslverify);
    }

    /**
    * Return binary user key (just needed for testing)
    *
    * @return mixed User key (binary)
    */
    public function getKey()
    {
        return self::$binUserKey;
    }

    /**
    * Set authorization credentials
    *
    * @param string $token $token = base64($id . $key . $padding)
    * @link https://wiki.skylable.com/wiki/REST_API_Primitives#User
    * @return boolean
    */
    public function setAuth($token)
    {
        $token = base64_decode($token, true);
        if ($token === '' || $token === null || strlen($token)<>42) {
            self::$binUserId = null;
            self::$binUserKey = null;
            return false;
        }

        self::$binUserId = substr($token, 0, 20);
        self::$binUserKey = substr($token, 20, 20);
        return true;
    }

    /**
    * Set Sx cluster endpoint
    *
    * @param string $endpoint
    * @param integer $port
    * @return string
    * @todo verify if endpoint is valid (tld or ip)
    */
    public function setEndpoint($endpoint, $port = null)
    {
        self::$endpoint = $endpoint;
        if ($port !== null) {
            self::$port = $port;
        }
        return self::$endpoint . ':' . self::$port;
    }

    /**
    * Set SX cluster tcp port
    *
    * @param integer $port
    * @return boolean
    */
    public function setPort($port)
    {
        if (is_int($port)) {
            return self::$port = $port;
        } else {
            return false;
        }
    }

    /**
    * Enable/disable SSL for endpoint connections
    *
    * @param boolean $ssl enable/disable ssl
    * @param boolean $verify verification of ssl certificates
    */
    public function setSSL($ssl, $verify = true)
    {
        if (($ssl === true || $ssl === false) && ($verify === true || $verify === false)) {
            self::$ssl = $ssl;
            self::$sslverify = $verify;
            return true;
        } else {
            return false;
        }
    }

    /**
    * List nodes
    *
    * API:
    *   Target: cluster
    *   Path: /?nodeList
    *
    * @link https://wiki.skylable.com/wiki/REST_API_List_Nodes
    */
    public function getNodeList()
    {
        $rest = $this->newRequest();
        $response = $rest->sendRequest('GET', '?nodeList');
        return json_decode($response);
    }

    /**
    * List volumes
    *
    * API:
    *   Target: cluster
    *   Path:  /?volumeList
    *
    * @link https://wiki.skylable.com/wiki/REST_API_List_Volumes
    */
    public function getVolumeList()
    {
        $rest = $this->newRequest();
        $response = $rest->sendRequest('GET', '?volumeList');
        return @json_decode($response, false, 512, JSON_BIGINT_AS_STRING);
    }

    /**
    * Get list of nodes responsible for volume
    *
    * API:
    *   Target: cluster
    *   Path: /$volume?o=locate&volumeMeta=1&size=123
    *
    * @param mixed $volume
    * @link https://wiki.skylable.com/wiki/REST_API_Locate_Volume
    * @todo FIXME('s) in skylable documentation, check for changes
    */
    public function locateVolume($volume, $size = false)
    {
        $rest = $this->newRequest();
        $req = $volume . '?o=locate&volumeMeta=1';
        if ($size !== false) {
            $req = $req . '&size=' . $size;
        }

        $response = $rest->sendRequest('GET', $req);
        return json_decode($response);
    }

    /**
    * List users
    *
    * @link https://wiki.skylable.com/wiki/REST_API_List_Users
    */
    public function getUserList()
    {
        $rest = $this->newRequest();
        return json_decode($rest->sendRequest('GET', '.users'));
    }

    /**
    * List files
    *
    * API:
    *   Target: VolumeNodes
    *   Path: /$volume?o=list[&filter=$pattern][&recursive]
    *
    * @param string $volume sx volume
    * @param string|false $filter filter pattern
    * @param boolean $recursive recursive query
    * @link https://wiki.skylable.com/wiki/REST_API_List_Files
    */
    public function getFileList($volume, $filter = false, $recursive = false)
    {
        $locate = $this->locateVolume($volume);
        $node = $locate->nodeList[0];

        $req = $volume . '?o=list';
        $req .= $filter ? '&filter=' . $filter : '';
        $req .= $recursive ? '&recursive' : '';

        $rest = $this->newRequest($node);
        return @json_decode($rest->sendRequest('GET', $req), false, 512, JSON_BIGINT_AS_STRING);
    }

    /**
    * Get file metadata
    *
    * API:
    *   Target: VolumeNodes
    *   Path: /$volume/$filepath?fileMeta
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @link https://wiki.skylable.com/wiki/REST_API_Get_File_Metadata
    */
    public function getFileMetadata($volume, $file)
    {
        $locate = $this->locateVolume($volume);
        $node = $locate->nodeList[0];

        $rest = $this->newRequest($node);
        $req = $volume . $rest->strPrefix($file) . '?fileMeta';
        return @json_decode($rest->sendRequest('GET', $req), false, 512, JSON_BIGINT_AS_STRING);
    }

    /**
    * Delete file
    *
    * API:
    *   Target: VolumeNodes
    *   Type: DELETE
    *   Path: /$volume/$filepath
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @link https://wiki.skylable.com/wiki/REST_API_Delete_File
    */
    public function deleteFile($volume, $file)
    {
        $locate = $this->locateVolume($volume);
        $node = $locate->nodeList[0];

        $rest = $this->newRequest($node);
        $req = $volume . $rest->strPrefix($file);
        $result = json_decode($rest->sendRequest('DELETE', $req));
        if (isset($result->requestId)) {
            return $this->getJobStatus($result->requestId, $result->minPollInterval, $result->maxPollInterval, 1);
        } else {
            throw new SxException('Unable to delete file', __FILE__, __LINE__, 0);
        }
    }

    /**
    * Get status of PUT/DELETE job
    *
    * @param integer $requestId ID of request
    * @param integer $minPollInterval minimal polling interval (microseconds)
    * @param integer $maxPollInterval maximum polling inteval (microseconds)
    * @param mixed $maxRetries maximum requests before returning control
    * @link https://wiki.skylable.com/wiki/REST_API_Replies#Reply_to_JOB_requests
    */
    public function getJobStatus($requestId, $minPollInterval, $maxPollInterval, $maxRetries = 10)
    {
        $req = '/.results/' . $requestId;
        $rest = $this->newRequest();
        $result = null;

        $delay = $minPollInterval;
        $delayIncr = floor(($maxPollInterval - $minPollInterval) / $maxRetries);
        $abortLoop = false;

        while (! $abortLoop) {
            usleep($delay);
            $delay += $delayIncr;
            $result = json_decode($rest->sendRequest('GET', $req));
            if ($result->requestStatus == 'OK' || $result->requestStatus == 'ERROR' || $delay > $maxPollInterval) {
                $abortLoop = true;
            }
        }
        return $result;
    }

    /**
    * Get file from volume (returns file)
    *
    * API:
    *   Target: VolumeNodes
    *   Path: /$volume/$filepath
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @link https://wiki.skylable.com/wiki/REST_API_Get_File
    * @todo enqueue blockkeys to retrieve more than one block a time
    */
    public function getFile($volume, $file)
    {
        $locate = $this->locateVolume($volume);
        $node = $locate->nodeList[0];

        $rawfile = null;
        $rest = $this->newRequest($node);
        $blocks = $rest->getFileBlocksMeta($volume, $file);

        $filesize = $blocks->fileSize;
        $blocksize = $blocks->blockSize;
        $nrblocks = ceil($filesize / $blocksize);

        foreach ($blocks->fileData as $block) {
            foreach ($block as $blockkey => $nodes) {
                $chunk = $rest->getSingleBlock($blockkey, $blocksize, $nodes, self::$port);
                $rawfile .= $chunk;
            }
        }
        return substr($rawfile, 0, $filesize);
    }

    /**
    * Get file from volume (stores file)
    *
    * API:
    *   Target: VolumeNodes
    *   Path: /$volume/$filepath
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @param string $target download target (full path and filename)
    * @return integer
    * @link https://wiki.skylable.com/wiki/REST_API_Get_File
    * @todo enqueue blockkeys to retrieve more than one block a time
    */
    public function downloadFile($volume, $file, $target)
    {
        $fp = @fopen($target, 'wb');
        if ($fp === false) {
            throw new \Exception('File open failed');
        }

        $locate = $this->locateVolume($volume);
        $node = $locate->nodeList[0];

        $rest = $this->newRequest($node);
        $blocks = $rest->getFileBlocksMeta($volume, $file);
        $openbytes = $blocks->fileSize;
        $fbytes = 0;

        foreach ($blocks->fileData as $block) {
            foreach ($block as $blockkey => $nodes) {
                $size = ($openbytes > $blocks->blockSize) ? $blocks->blockSize : $openbytes;
                $chunk = $rest->getSingleBlock($blockkey, $blocks->blockSize, $nodes, self::$port);
                $result = @fwrite($fp, $chunk, $size);
                if ($result == false) {
                    unlink($target);
                    throw new \Exception('File write failed');
                }
                $fbytes += $result;
                $openbytes -= $size;
            }
        }

        fclose($fp);
        return $fbytes;
    }

    /**
    * Put file into volume (source = php variable)
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @param string $body file content
    * @param mixed $metadata
    * @link https://wiki.skylable.com/wiki/REST_API_Initialize_File
    * @link https://wiki.skylable.com/wiki/REST_API_Create_Blocks
    * @link https://wiki.skylable.com/wiki/REST_API_Flush_File
    * @link https://wiki.skylable.com/wiki/REST_API_Initialize_Add_Chunk
    * @link https://wiki.skylable.com/wiki/REST_API_Locate_Volume
    * @todo FIXME('s) in skylable documentation, check for changes
    * @todo enqueue blocks to send more than one block a time
    * @todo use chunk api for files larger>128mb
    */
    public function putFile($volume, $file, $body, $metadata = null)
    {
        $rest = $this->newRequest();

        $filesize = strlen($body);
        $nodes = $this->locateVolume($volume, $filesize);
        $blocksize = $nodes->blockSize;
        $blockcnt = ceil($filesize / $blocksize);
        $body = str_pad($body, $blockcnt * $blocksize, chr(0));

        $payload = array(
            'fileSize' => $filesize
        );

        if ($metadata !== null) {
            // @todo: meta data handling
            $payload['fileMeta'] = (object) $metadata;
            throw new \Exception('Metadata not implemented');
        }

        $blockhash = array();
        for ($i=0; $i<$blockcnt; $i++) {
            $block = substr($body, $i * $blocksize, $blocksize);
            $hash = sha1($rest->getClusterId() . $block);
            $blockhash[$hash] = $i;
            $payload['fileData'][] = $hash;
        }

        // initialize
        $req = $volume . $rest->strPrefix($file);
        $result = json_decode(
            $rest->sendRequest('PUT', $req, json_encode($payload), $nodes->nodeList[0] . ':' . self::$port)
        );
        if ($result == false) {
            throw new SxException('File upload initialization failed', __FILE__, __LINE__, 0);
        }
        $token = $result->uploadToken;

        // block upload
        foreach ($result->uploadData as $block => $blocknodes) {
            $block = substr($body, $blockhash[$block] * $blocksize, $blocksize);
            $req = '.data/' . $blocksize . '/' . $token;
            $result = json_decode($rest->sendRequest('PUT', $req, $block, $blocknodes[0] . ':' . self::$port));
        }

        // flush
        $req = '/.upload/' . $token;
        $result = json_decode($rest->sendRequest('PUT', $req, null, $nodes->nodeList[0] . ':' . self::$port));
        if (! isset($result->requestId)) {
            throw new SxException('Unable to flush file', __FILE__, __LINE__, 0);
        }
        $result = $this->getJobStatus($result->requestId, $result->minPollInterval, $result->maxPollInterval, 3);
        return $result;
    }

    /**
    * Upload file into volume (source = filesystem)
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @param string $source upload target (full path and filename)
    * @param mixed $metadata
    * @link https://wiki.skylable.com/wiki/REST_API_Initialize_File
    * @link https://wiki.skylable.com/wiki/REST_API_Create_Blocks
    * @link https://wiki.skylable.com/wiki/REST_API_Flush_File
    * @link https://wiki.skylable.com/wiki/REST_API_Initialize_Add_Chunk
    * @link https://wiki.skylable.com/wiki/REST_API_Locate_Volume
    * @todo FIXME('s) in skylable documentation, check for changes
    * @todo enqueue blocks to send more than one block a time
    * @todo use chunk api for files larger>128mb
    */
    public function uploadFile($volume, $file, $source, $metadata = null)
    {
        if (!file_exists($source)) {
            throw new \Exception('File not found');
        }
        $filesize = @filesize($source);
        if ($filesize === false) {
            throw new \Exception('Cannot stat file');
        }

        $rest = $this->newRequest();

        $nodes = $this->locateVolume($volume, $filesize);
        $blocksize = $nodes->blockSize;
        $blockcnt = ceil($filesize / $blocksize);

        $payload = array(
            'fileSize' => $filesize
        );

        if ($metadata !== null) {
            // @todo: meta data handling
            $payload['fileMeta'] = (object) $metadata;
            throw new \Exception('Metadata not implemented');
        }

        $i = 0;
        $blockhash = array();
        $fh = fopen($source, 'rb');
        while (!feof($fh)) {
            $block = fread($fh, $blocksize);
            if (strlen($block) < $blocksize) {
                $block = str_pad($block, $blocksize, chr(0));
            }
            $hash = sha1($rest->getClusterId() . $block);
            $blockhash[$hash] = $i++;
            $payload['fileData'][] = $hash;
        }
        fclose($fh);

        // initialize
        $req = $volume . $rest->strPrefix($file);
        $result = json_decode(
            $rest->sendRequest('PUT', $req, json_encode($payload), $nodes->nodeList[0] . ':' . self::$port)
        );
        if ($result == false) {
            throw new SxException('File upload initialization failed', __FILE__, __LINE__, 0);
        }
        $token = $result->uploadToken;

        // identify required blocks
        $blocktargets = array();
        foreach ($result->uploadData as $block => $blocknodes) {
            $blocktargets[$block] = $blocknodes;
        }
        foreach ($blockhash as $hash => $val) {
            if (!isset($blocktargets[$hash])) {
                unset ($blockhash[$hash]);
            }
        }

        // block upload
        $cnt = 0;
        $fh = fopen($source, 'rb');
        foreach ($blockhash as $hash => $blocknr) {
            while ($cnt < $blocknr) {
                $block = fread($fh, $blocksize);
                $cnt++;
            }
            $block = fread($fh, $blocksize);

            if (strlen($block) < $blocksize) {
                $block = str_pad($block, $blocksize, chr(0));
            }

            $req = '.data/' . $blocksize . '/' . $token;
            $result = json_decode($rest->sendRequest('PUT', $req, $block, $blocknodes[0] . ':' . self::$port));
            $cnt++;
        }
        fclose($fh);

        // flush
        $req = '/.upload/' . $token;
        $result = json_decode($rest->sendRequest('PUT', $req, null, $nodes->nodeList[0] . ':' . self::$port));
        if (! isset($result->requestId)) {
            throw new SxException('Unable to flush file', __FILE__, __LINE__, 0);
        }
        $result = $this->getJobStatus($result->requestId, $result->minPollInterval, $result->maxPollInterval, 3);
        return $result;
    }
}
