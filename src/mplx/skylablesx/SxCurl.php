<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx;

/**
* SxCurl class
*/
class SxCurl
{
    /**
    * curl handle
    *
    * @var mixed
    */
    protected $ch;

    /**
    * full uri
    *
    * @var mixed
    */
    private $uri = '';

    /**
    * last request HTTP header
    *
    * @var mixed
    */
    private $header_request = null;

    /**
    * last response HTTP header
    *
    * @var mixed
    */
    private $header_response = null;

    /**
    * initialize curl session w/ default options
    *
    * @param mixed $uri
    * @param boolean $sslverify
    * @return boolean
    */
    public function __construct($uri = false, $sslverify = null)
    {
        $this->ch = curl_init();
        if ($this->ch === false) {
            throw new SxException('cURL init failed', __FILE__, __LINE__, 0);
        }

        $result = curl_setopt($this->ch, CURLOPT_VERBOSE, 0);
        $result = $result && curl_setopt($this->ch, CURLOPT_USERAGENT, 'mplx/skylable-sx-php');
        $result = $result && curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        $result = $result && curl_setopt($this->ch, CURLOPT_HEADER, 1);
        $result = $result && curl_setopt($this->ch, CURLINFO_HEADER_OUT, 1);
        $result = $result && curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = $result && curl_setopt($this->ch, CURLOPT_MAXREDIRS, 3);

        if ($uri) {
            $this->uri = $uri;
            $result = $result && $this->setUri($uri);
        }

        if (isset($sslverify)) {
            $result = $result && $this->setSSLverify($sslverify);
        }

        return $result;
    }

    /**
    * enable/disable SSL certificate verification
    *
    * @param boolean $verify
    * @return boolean
    */
    public function setSSLverify($verify)
    {
        if ($verify) {
            $result = curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);
            $result = $result && curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 1);
        } else {
            $result = curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
            $result = $result && curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        return $result;
    }

    /**
    * set custom request method
    *
    * @param string $method
    * @return boolean
    */
    public function setRequestMethod($method)
    {
        if ($method === 'GET') {
            $return = curl_setopt($this->ch, CURLOPT_HTTPGET, 1);
        } elseif ($method === 'POST') {
            $return = curl_setopt($this->ch, CURLOPT_POST, 1);
        } elseif ($method === 'PUT') {
            // $return = curl_setopt($this->ch, CURLOPT_PUT, 1);
            $return = curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
        } elseif ($method === 'DELETE') {
            $return = curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
        } else {
            throw new SxException('Unsupported request method', __FILE__, __LINE__, 0);
        }
        return $return;
    }

    /**
    * set URI
    *
    * @param string $uri
    * @return boolean
    */
    public function setUri($uri)
    {
        $return = curl_setopt($this->ch, CURLOPT_URL, $uri);
        return $return;
    }

    /**
    * set HTTP request header
    *
    * @param mixed $header
    * @return boolean
    */
    public function setHeader($header)
    {
        $return = curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
        return $return;
    }

    /**
    * get HTTP response header
    *
    * @return string
    * @return boolean
    */
    public function getHeader()
    {
        return $this->header_response;
    }

    /**
    * set HTTP request body (postfields)
    *
    * @param string $body
    * @return boolean
    */
    public function setBody($body)
    {
        $return = curl_setopt($this->ch, CURLOPT_POSTFIELDS, $body);
        return $return;
    }

    /**
    * tell cURL we're uploading
    *
    * @param boolean $flag
    * return bool
    */
    public function setUpload($flag = true)
    {
        $return = curl_setopt($this->ch, CURLOPT_UPLOAD, true);
        return $return;
    }

    /**
    * perform cURL session, return response body
    *
    * @return mixed
    */
    public function exec()
    {
        $response = curl_exec($this->ch);

        $this->header_request = curl_getinfo($this->ch, CURLINFO_HEADER_OUT);
        $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);

        if ($response !== false) {
            $this->header_response = substr($response, 0, $header_size);
        }
        if (curl_errno($this->ch) > 0 || $response === false) {
            return false;
        }

        if (strlen($response) > $header_size) {
            $body = substr($response, $header_size);
        } else {
            $body = '';
        }
        return $body;
    }

    /**
    * get HTTP response header
    *
    * @return mixed
    */
    public function getResponseHeader()
    {
        return $this->header_response;
    }

    /**
    * get HTTP response code
    *
    * @return mixed
    */
    public function getResponseCode()
    {
        return curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    }

    /**
    * get CURL error
    *
    * @return mixed
    */
    public function getError()
    {
        return curl_error($this->ch);
    }

    /**
    * close curl session
    */
    public function close()
    {
        curl_close($this->ch);
    }
}
