<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx;

/**
 * Sx Exception class
 */
class SxException extends \Exception
{
    public function __construct($message, $file, $line, $code)
    {
        parent::__construct($message . " [$line@$file]", $code);
    }
}
