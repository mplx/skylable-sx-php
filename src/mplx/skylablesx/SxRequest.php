<?php
/**
 * Skylable SX PHP Client
 *
 * @package     skylablesx
 * @author      Martin Pircher <mplx+coding@donotreply.at>
 * @copyright   Copyright (c) 2014-2015, Martin Pircher
 * @license     http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 **/

namespace mplx\skylablesx;

/**
* Sx Request class
*/
class SxRequest
{
    /**
    * SX user id (binary presentation)
    *
    * @var string
    */
    private static $binUserId;

    /**
    * SX user key (binary presentation)
    *
    * @var string
    */
    private static $binUserKey;

    /**
    * Cluster or node endpoint
    *
    * @var string
    */
    private static $endpoint;

    /**
    * SX Cluster UUID
    *
    * @var string
    */
    public static $clusterid = '';

    /**
    * Use SSL for SX connection
    *
    * @var boolean
    */
    private static $ssl;

    /**
    * Verify certificates in SSL connections
    *
    * @var boolean
    */
    private static $sslverify;

    /**
    * SX key padding
    *
    * @var string
    */
    private static $padding;

    /**
    * Constructor
    *
    * @param string $binUserId User ID (binary)
    * @param string $binUserKey User Key (binary))
    * @param string $endpoint cluster or node endpoint, optional with tcp port
    * @param boolean $ssl use SSL
    * @param boolean $sslverify verify SSL certificates
    * @param string $padding Key padding
    */
    public function __construct($binUserId, $binUserKey, $endpoint, $ssl, $sslverify, $padding = '0000')
    {
        self::$binUserId = $binUserId;
        self::$binUserKey = $binUserKey;
        self::$endpoint = $endpoint;
        self::$ssl = $ssl;
        self::$sslverify = $sslverify;
        self::$padding = $padding;
    }

    /**
    * Perform a SX REST request with cURL
    *
    * @param string $verb GET/PUT/DELETE/etc
    * @param string $path SX path
    * @param string $body request body
    */
    public static function sendRequest($verb, $path, $body = '', $node = null)
    {
        $returncode = null;
        $node = ($node === null) ? self::$endpoint : $node;

        if (self::$ssl) {
            $uri = 'https://' . $node . '/' . $path;
        } else {
            $uri = 'http://' . $node . '/' . $path;
        }

        $curl = new SxCurl($uri, self::$sslverify);

        $date = gmdate('D, d M Y H:i:s T');
        $signkey = self::signRequest($verb, $path, $date, sha1($body));
        $curl->setRequestMethod($verb);

        $header = array(
            self::createContentHeader($uri),
            self::createDateHeader($date),
            self::createAuthHeader($signkey)
        );
        $curl->setHeader($header);

        if ($body !== null && $body !== '') {
            $curl->setBody($body);
            $header[]='Content-Length: ' . strlen($body);
            // @todo: I don't think we need this?
            /*
            if (self::createContentHeader($uri) == 'application/octet-stream') {
                $curl->setUpload();
            }
            */
        }

        $response = $curl->exec();
        if ($response === false) {
            throw new SxException('cURL Error: ' . $curl->getError(), __FILE__, __LINE__, 0);
        }

        $returncode = $curl->getResponseCode();
        if ($returncode <> 200) {
            throw new SxException('HTTP code: ' . $returncode, __FILE__, __LINE__, $returncode);
        }

        $curl->close();

        if (self::$clusterid == '') {
            $header = $curl->getResponseHeader();
            $result = preg_match('/^SX-Cluster: ([0-9]+\.[0-9]+) \(([a-z0-9-]+)\)/m', $header, $matches);
            if ($result === false || $result < 1) {
                throw new SxException('Missing HTTP SX-Cluster header', __FILE__, __LINE__, 0);
            } else {
                self::$clusterid = $matches[2];
            }
        }

        return $response;
    }

    /**
    * Sign request
    *
    * @param string $verb i.e. "GET"
    * @param string $path i.e. "?nodeList"
    * @param mixed $date i.e. "Thu, 10 Jul 2014 13:35:32 GMT"
    * @param string $bodysha1 SHA1 checksum of body (hexadecimal)
    * @return string
    * @link https://wiki.skylable.com/wiki/REST_API_Queries#How_to_sign_a_query
    */
    public static function signRequest($verb, $path, $date, $bodysha1)
    {
        $hmac = self::hmacSha1(
            self::$binUserKey,
            $verb . "\n" . $path . "\n" .$date . "\n" . $bodysha1 . "\n"
        );

        // @todo: remove line below
        // dump($verb . "\n" . $path . "\n" .$date . "\n" . $bodysha1 . "\n");

        $signkey = bin2hex(self::$binUserId) . $hmac . self::$padding;
        return $signkey;
    }

    /**
    * Calculate hmac_sha1
    *
    * @param strin $key raw key
    * @param string $data data payload
    * @return string
    */
    public static function hmacSha1($key, $data)
    {
        $hmac = hash_hmac('sha1', $data, $key, false);
        return $hmac;
    }

    /**
    * Create HTTP authorization header
    *
    * @param string $signKey SX signing key
    * @param string $prefix Authorization prefix (defaults to 'SKY')
    * @return string
    */
    public static function createAuthHeader($signKey, $prefix = 'SKY')
    {
        return 'Authorization: ' . $prefix . ' '. base64_encode(hex2bin($signKey));
    }

    /**
    * Create HTTP content type header
    *
    * @param string $uri
    * @return string
    */
    public static function createContentHeader($uri)
    {
        if (strpos($uri, '/.data/')) {
            return 'Content-Type: ' . 'application/octet-stream';
        } else {
            return 'Content-Type: ' . 'application/json';
        }
    }

    /**
    * Create HTTP date header
    *
    * @param string $date
    * @return string
    */
    public static function createDateHeader($date)
    {
        return 'Date: ' . $date;
    }

    /**
    * Get file blocks meta data
    *
    * @param string $volume sx volume
    * @param string $file file (full path and filename)
    * @link https://wiki.skylable.com/wiki/REST_API_Get_File
    */
    public function getFileBlocksMeta($volume, $file)
    {
        $req = $volume . self::strPrefix($file);
        return json_decode(self::sendRequest('GET', $req));
    }

    /**
    * Get single block from sx node
    *
    * @param string $blockname blockname
    * @param string $blocksize blocksize (bytes)
    * @param array $nodes array of nodes (ip addresses) where block is avaible
    * @link https://wiki.skylable.com/wiki/REST_API_Get_Blocks
    * @todo use best node, handle node failures
    */
    public function getSingleBlock($blockname, $blocksize, $nodes, $port)
    {
        $req = '.data/' . $blocksize . '/' . $blockname;
        return $this->sendRequest('GET', $req, '', $nodes[0] . ':' . $port);
    }

    /**
    * Get SX cluster UUID
    */
    public function getClusterId()
    {
        if (self::$clusterid != '') {
            return self::$clusterid;
        } else {
            // we need some request response... nodelist seems cheap?
            $nodes = $this->sendRequest('GET', '?nodeList');
        }
    }

    /**
    * Add leading slash to string
    *
    * @param string $str string to be prefixed
    */
    public static function strPrefix($str, $prefix = '/')
    {
        if (isset($str[0]) && ($str[0] === $prefix || $str[0] === '?')) {
            return $str;
        } else {
            return $prefix . $str;
        }
    }

    /**
    * Block padding
    *
    * @param string $block
    * @param integer $blocksize Size of single block in bytes
    * @param string
    * @return string
    */
    public static function blockPadding($block, $blocksize, $char = null)
    {
        if ($char === null) {
            $char = chr(0);
        }

        $padding = $blocksize - strlen($block);

        if ($padding === 0) {
            return $block;
        }
        if ($padding < 0) {
            throw new SxException('Block to large', __FILE__, __LINE__, 0);
        }

        // @todo: replace with str_pad() in case it's faster
        for ($i=0; $i < $padding; $i++) {
            $block .= $char;
        }

        return $block;
    }

    public function setEndpoint($node)
    {
        self::$endpoint = $node;
        return true;
    }
}
